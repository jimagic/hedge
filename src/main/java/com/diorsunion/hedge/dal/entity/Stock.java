package com.diorsunion.hedge.dal.entity;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author harley-dog on 2015/6/4.
 */
@Entity
public class Stock implements Serializable {
    @Id
    public String code;
    public String name;
    public String exchange;
    public String url;
    public Date syncEndDate;//同步截至日期
    public Map<Date, StockPrice> stockPriceMap = Maps.newLinkedHashMap();//股票每天的价格

    public long totalShares;//总股本
    public long marketCapital;//总市值

    public Stock(String code) {
        this.code = code;
    }

    public Stock() {
    }

    public List<StockPrice> getSortedStockPriceList() {
        List<StockPrice> stockPriceList = Lists.newArrayList();
        stockPriceList.addAll(stockPriceMap.values());
        stockPriceList.sort((a,b)->a.thedate.compareTo(b.thedate));
        return stockPriceList;
    }

    public StockPrice getStockPrice(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return stockPriceMap.get(calendar.getTime());
    }

    public void setStockPrice(Date date, StockPrice stockPrice) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        stockPriceMap.put(calendar.getTime(), stockPrice);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stock stock = (Stock) o;

        return code.equals(stock.code);

    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    public String toString() {
        return "Stock{" +
                "code='" + code + '\'' +
                '}';
    }
}
