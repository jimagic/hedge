package com.diorsunion.hedge.dal.entity;

import com.diorsunion.hedge.util.CalendarUtils;

import java.util.Date;

/**
 * 交易日志
 * Created by wanglaoshi on 2016/4/16.
 */
public class TradeLog {
    public Stock stock;
    public int num;
    public double price;
    public TradeType tradeType;
    public double money;
    public Date date;
    public int position;
    public double total;


    public TradeLog(Stock stock, int num, double price, TradeType tradeType, double money, Date date, int position, double total) {
        this.stock = stock;
        this.num = num;
        this.price = price;
        this.tradeType = tradeType;
        this.money = money;
        this.date = date;
        this.position = position;
        this.total = total;
    }

    @Override
    public String toString() {
        return "TradeLog{" +
                "stock=" + stock +
                ", num=" + num +
                ", money=" + money +
                ", total=" +String.format("%.2f", total)  +
                ", date=" + CalendarUtils.formatSimple(date) +
                '}';
    }


    public String format() {
        return CalendarUtils.formatSimple(date)+" "+tradeType.desc+" "+Math.abs(num)+"股"+stock.code+",单价:"+String.format("%.2f", price)+",总金额"+ String.format("%.2f", money)+",持仓:"+position+",净资产"+ String.format("%.2f", total);
    }
}
