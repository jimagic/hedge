package com.diorsunion.hedge.dal.entity;

import com.diorsunion.hedge.domain.Trend;

/**
 * 一些扩展字段
 * Created by wanglaoshi on 2016/4/17.
 */
public class StockPriceEx {
    public double ema12;
    public double ema26;
    public double dif;
    public double dea;
    public double macd;
    public Trend macdTrend;//macd趋势
    public double price;//第二天的MACD值不发生变化的临界点价格
    public double k;
    public double d;
    public double j;
    public double rsi1;
    public double rsi2;
    public double rsi3;
    public double bollUp;
    public double bollMix;
    public double bollLow;

}
