package com.diorsunion.hedge.common;

/**
 * Created by wanglaoshi on 2016/4/23.
 */
public final class ServiceResult {
    public String code;
    public boolean success;
    public String message;

    public final static ServiceResult returnResult(boolean success,String message){
        return new ServiceResult("",success,message);
    }
    public final static ServiceResult returnResult(String code,boolean success,String message){
        return new ServiceResult(code,success,message);
    }

    private ServiceResult(String code, boolean success, String message) {
        this.code = code;
        this.success = success;
        this.message = message;
    }
}
