package com.diorsunion.hedge.util;

/**
 * @author harley-dog on 2015/6/5.
 */
public final class CurrencyUtils {

    public static String format(double x){
        return String.format("%.2f",x);
    }
}
