package com.diorsunion.hedge.util;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author harley-dog on 2015/6/4.
 */
public final class CalendarUtils {

    public final static DateFormat dateFormat_US = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
    private final static DateFormat dateFormat_full = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS E");
    private final static DateFormat dateFormat_standard = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final static Lock lock_full = new ReentrantLock();
    private final static Lock lock_simple = new ReentrantLock();

    public static String formatStandard(Date date) {
        return format(date,dateFormat_standard);
    }

    public static String formatFull(Date date) {
        return format(date,dateFormat_full);
    }

    public static String formatSimple(Date date) {
        return format(date,dateFormat);
    }

    private static String format(Date date,DateFormat dateFormat){
        String s = null;
        lock_simple.lock();
        s = dateFormat.format(date);
        lock_simple.unlock();
        return s;
    }

    public static Date parseSimple(String str) throws ParseException {
        Date date = null;
        lock_simple.lock();
        date = dateFormat.parse(str);
        lock_simple.unlock();
        return date;
    }

    /**
     * 返回某个date,指定某个字段(比如月,星期)的第一天
     *
     * @param date  2015-01-05 12:13:14.234
     * @param date  2015-01-05 12:13:14.234
     * @param field Calendar.MONTH
     * @return 2015-01-01 00:00:00.000
     */
    public static Date getMondayDate(Date date, int field) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(field, 1);
        return c.getTime();
    }

    /**
     * 返回昨天
     *
     * @e.g current time:2015-01-05 12:13:14.234
     * @return 2015-01-04 00:00:00.000
     */
    public static Date getYestoday() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DATE, -1);
        return c.getTime();
    }

    /**
     * 返回两个时间制定字段的差值
     *
     * @param begin 2015-01-05 12:13:14.234
     * @param end   2015-01-06 12:13:14.234
     * @param field Calendar.Day
     * @return 1
     */
    public static int getDiff(Date begin, Date end, int field) {
        int r = 0;
        switch (field) {
            case Calendar.MILLISECOND:
                return (int) (end.getTime() - begin.getTime());
            case Calendar.SECOND:
                return (int) ((end.getTime() - begin.getTime()) / 1000);
            case Calendar.MINUTE:
                return (int) ((end.getTime() - begin.getTime()) / 60000);
            case Calendar.HOUR:
                return (int) ((end.getTime() - begin.getTime()) / 3600000);
            case Calendar.DATE:
                return (int) ((end.getTime() - begin.getTime()) / 86400000);
            default:
                return (int) ((end.getTime() - begin.getTime()) / 86400000);
        }
    }

    public static Date addDate(Date date, int days) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DATE, days);
        return c.getTime();
    }

    /**
     * 返回当前时间加上若干天后的date对象
     *
     * @param days 3
     * @return 2015-01-05 00:00:00.000
     * @e.g current time:2015-01-05 12:13:14.234
     */
    public static Date addDate(int days) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DATE, days);
        return c.getTime();
    }

    public static String getDateFormat(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    public static void main(String[] args) throws ParseException {
        Date date = new Date();
        Date d = getMondayDate(date, Calendar.DAY_OF_WEEK);
        System.out.println(d);

        Date yes = getYestoday();
        System.out.println(yes);

        DateFormat dateFormat_cn = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        Date d2000 = dateFormat_cn.parse("2000-01-01");
        System.out.println(d2000 + ":" + d2000.getTime());
    }
}
