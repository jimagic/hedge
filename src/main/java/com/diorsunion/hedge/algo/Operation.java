package com.diorsunion.hedge.algo;

import com.diorsunion.hedge.dal.entity.Account;
import com.diorsunion.hedge.dal.entity.TradeLog;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * 实现这个接口，进行股票的虚拟买卖操作 ，目的是提高净值
 *
 * @author harley-dog on 2015/7/22.1
 */
public abstract class Operation {

    public static final String N = "N";//天数
    public static final String PROFIT = "P";//止盈率
    public static final String LOSS = "L";//止损率
    public static final String DLOSS = "DL";//动态止损率
    /**
     * 可以由外界传入指定的参数,进行调优
     */
    public final Map<String, Integer> params = Maps.newHashMap();
    public List<TradeLog> tradeLogs = Lists.newArrayList();

    public Operation(Map<String, Integer> params) {
        if (params != null) {
            this.params.putAll(params);
        }
    }

    /**
     * 操作股票
     *
     * @param account          当前操作账户
     * @param account_per_days 历史每天账户
     */
    public abstract void oper(Account account, List<Account> account_per_days);

    public abstract String getDesc();

    protected String printMoney(double d) {
        return String.format("%.2f", d);
    }


    //按顺序打印交易日志
    public void printTradeLog() {
        tradeLogs.forEach(tradeLog -> System.out.println(tradeLog.format()));
    }

    //按股票打印交易日志
    public void printTradeLogOrderByDate() {
        if (!tradeLogs.isEmpty() && tradeLogs.size() > 2) {
            List<TradeLog> sortedList = Lists.newArrayList();
            sortedList.addAll(tradeLogs);
            sortedList.sort((a, b) -> a == null || b == null ? 0 : a.stock.code.compareTo(b.stock.code));
            sortedList.stream()
                    .filter(x -> x != null)
                    .forEach(tradeLog -> System.out.println(tradeLog.format()));
        }
    }
}
