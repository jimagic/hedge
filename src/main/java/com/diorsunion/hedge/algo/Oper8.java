package com.diorsunion.hedge.algo;

import com.diorsunion.hedge.dal.entity.Account;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;

import java.util.List;
import java.util.Map;

/**
 * 传说中的反向对冲，用做空的方式来对冲
 * @author harley-dog on 2015/7/22.
 */
public class Oper8 extends Operation {
    private Account account_period_begin = null;

    private StockPrice.PriceType priceType = StockPrice.PriceType.CLOSE;//用收盘价来计算

    public Oper8(Map<String, Integer> params) {
        super(params);
    }

    @Override
    public void oper(Account account, List<Account> account_per_days) {
        //第一天,均衡卖空，两边都卖空 balance/2 这么多股票
        if (account_per_days.size() == 1) {
            int buy_count = account.stockPool.size();
            double balance = account.balance;
            for (Stock stock : account.stockPool.keySet()) {
                account.sell(stock, balance / buy_count, priceType);
            }
            account_period_begin = account;
            return;
        }
        if (account_per_days.size() > 2) {
            Stock stock_high = account.getHighest(priceType);
            Stock stock_low = account.getLowest(priceType);
            Account account_yestoday = account_per_days.get(account_per_days.size() - 2);

            double high = account.getStockValue(stock_high,StockPrice.PriceType.CLOSE);
            double high_yestoday = account_yestoday.getStockValue(stock_high, StockPrice.PriceType.CLOSE);


            double diff = high-high_yestoday;
            account.buy(stock_high,diff, StockPrice.PriceType.CLOSE);
            account.sell(stock_low, diff, StockPrice.PriceType.CLOSE);
        }
    }

    @Override
    public String getDesc() {
        return " 对冲算法1.3版本\n" +
                " * 中心思想是\n" +
                " * 1.首先均等买入做多和做空两股,计算为一个周期的开始\n" +
                " * 2.然后每经过一个周期,当涨跌总是单一反向涨跌时,总值会增长\n" +
                " * 3.当总值增长比率 超过指定的 百分比后,进行操作\n" +
                " * 4.操作方式是 将涨股 超过跌股的部分的一半卖掉,补入跌股\n" +
                " * 5.当进行一次操作后,周期重新开始计算";
    }

}
