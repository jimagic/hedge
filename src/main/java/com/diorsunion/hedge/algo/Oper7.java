package com.diorsunion.hedge.algo;

import com.diorsunion.hedge.dal.entity.Account;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;

import java.util.List;
import java.util.Map;

/**
 * 传说中的反向对冲，用做空的方式来对冲
 * @author harley-dog on 2015/7/22.
 */
public class Oper7 extends Operation {
    private Account account_period_begin = null;

    private StockPrice.PriceType priceType = StockPrice.PriceType.CLOSE;//用收盘价来计算

    public Oper7(Map<String, Integer> params) {
        super(params);
    }

    @Override
    public void oper(Account account, List<Account> account_per_days) {
        //第一天,均衡卖空，两边都卖空 balance/2 这么多股票
        if (account_per_days.size() == 1) {
            int buy_count = account.stockPool.size();
            double balance = account.balance;
            for (Stock stock : account.stockPool.keySet()) {
                account.sell(stock, balance / buy_count, priceType);
            }
            account_period_begin = account;
            return;
        }
//        if (account_per_days.size() > 2) {
//            double total_current = account.getTotalValue(priceType);//当天总值
//            double total_period = account_period_begin.getTotalValue(priceType);//周期开始的总值
//            //r为两个负值之间的差距
//            BigDecimal r = new BigDecimal((total_current - total_period) * 100 / total_period).setScale(2, BigDecimal.ROUND_HALF_UP);
//            Integer profit = params.get(PROFIT);
//            Integer loss = params.get(LOSS);
//            if (r.intValue() >= profit) {
//                System.out.println("上一个周期开始:" + CalendarUtils.formatSimple(account_period_begin.date) +
//                        ",资产净值:" + account_period_begin.getTotalValueStr(priceType) +
//                        ",当前资产净值" + account.getTotalValueStr(priceType) +
//                        ",收益率达到" + String.format("%2.2f", r.doubleValue()) +
//                        "%,超过预期的" + profit +
//                        "%,开始进行止盈操作");
//                Stock stock_high = account.getHighest(priceType);
//                Stock stock_low = account.getLowest(priceType);
//                double value_high = account.getStockValue(stock_high, StockPrice.PriceType.CLOSE);//得到股票净值
//                double value_low = account.getStockValue(stock_low, StockPrice.PriceType.CLOSE);//得到周期开始的股票净值
//                double diff = value_high - value_low;//计算获利
//                account.buy(stock_low, diff / 2, StockPrice.PriceType.CLOSE);//用获利的一半用于买入价值低的股票
//                account.sell(stock_high, diff / 2, StockPrice.PriceType.CLOSE);//把获利的一半卖出再卖出价值高的股票
//                account_period_begin = account;
//            } else if (r.intValue() <= loss) {
//                System.out.println("上一个周期开始:" + CalendarUtils.formatSimple(account_period_begin.date) +
//                        ",资产净值:" + account_period_begin.getTotalValueStr(priceType) +
//                        ",当前资产净值" + account.getTotalValueStr(priceType) +
//                        ",损失率达到" + String.format("%2.2f", r.doubleValue()) +
//                        "%,超过预期的" + profit +
//                        "%,开始进行止损操作");
//                Stock stock_high = account.getHighest(priceType);
//                Stock stock_low = account.getLowest(priceType);
//                double value_high = account.getStockValue(stock_high, StockPrice.PriceType.CLOSE);//得到股票净值
//                double value_low = account.getStockValue(stock_low, StockPrice.PriceType.CLOSE);//得到周期开始的股票净值
//                double diff = value_high - value_low;//计算损失
//                account.buy(stock_low, diff / 2, StockPrice.PriceType.CLOSE);//用获利的一半用于买入价值低的股票
//                account.sell(stock_high, diff / 2, StockPrice.PriceType.CLOSE);//把获利的一半卖出再卖出价值高的股票
//                account_period_begin = account;
//            }
//        }
    }

    @Override
    public String getDesc() {
        return " 反向对冲算法1.0版本\n" +
                " * 中心思想是\n" +
                " * 1.首先均等沽空做多和做空两股,计算为一个周期的开始\n" +
                " * 2.然后啥也不干\n" ;
    }

}
