package com.diorsunion.hedge.bo.quota;

import com.diorsunion.hedge.dal.entity.StockPrice;

import java.util.List;

/**
 * 辅助指数计算器
 * Created by wanglaoshi on 2016/4/17.
 */
public interface StockPriceFilter {
    void filter(List<StockPrice> stockPriceList);
}
