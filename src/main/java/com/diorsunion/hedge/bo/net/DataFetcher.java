package com.diorsunion.hedge.bo.net;

import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by harley-dog<custer7572@163.com> on 15/12/15.
 */
public interface DataFetcher {
    /**
     * 从远程抓取股票数据
     *
     * @param stock 要抓取哪只股票
     * @param begin 抓取数据的起始时间
     * @param end   抓取数据的截止时间
     * @return
     */
    List<StockPrice> fetchStockPrice(Stock stock, Date begin, Date end, Period period) throws IOException, URISyntaxException, ParseException;


    default String getCookie(URI uri) throws IOException {
        StringBuilder cookie = new StringBuilder();
        HttpGet httpget = new HttpGet(uri);
        CloseableHttpClient httpclient = HttpClients.custom().build();
        CloseableHttpResponse response = httpclient.execute(httpget);
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if ("Set-Cookie".equals(header.getName())) {
                cookie.append(header.getValue()).append(";");
            }
        }
        return cookie.toString();
    }

}
