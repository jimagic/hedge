package com.diorsunion.hedge.bo.net;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.diorsunion.hedge.common.Constants;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import com.diorsunion.hedge.web.util.LogUtil;
import com.google.common.collect.Lists;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by harley-dog<custer7572@163.com> on 15/12/15.
 */
@Service("XueQiuDataFetcher")
public class XueQiuDataFetcher implements DataFetcher {

    @Value("#{config['xueqiu_stock_price_url']}")
    String xueqiu_stock_price_url;

    /*TODO cookie这个东西，还是得搞个简单的缓存啊，一天刷新一次就可以了哦*/
    private long time = 0 ;
    private String cookie_cached ;

    @Override
    public List<StockPrice> fetchStockPrice(Stock stock, Date begin, Date end, Period period) throws IOException, URISyntaxException, ParseException {
        DateFormat dateFormat_US = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
        DateFormat dateFormat_CN = new SimpleDateFormat("yyyy-MM-dd");
        LogUtil.debug("fetchStockPrice,stock:" + stock.code + ",begin:" + dateFormat_CN.format(begin) + ",end:" + dateFormat_CN.format(end) + ",peroid:" + period);
        URIBuilder uriBuilder = new URIBuilder(xueqiu_stock_price_url)
                .setParameter("type", "normal")
                .setParameter("_", String.valueOf(System.currentTimeMillis()))
                .setParameter("begin", String.valueOf(begin.getTime()))
                .setParameter("end", String.valueOf(end.getTime()))
                .setParameter("period", getPeroidParam(period))
                .setParameter("symbol", stock.code);
        URI uri = uriBuilder.build();
        URL url = uri.toURL();
        URI root_uri = new URI("http://" + uri.getHost());
        String cookie = getCookie(root_uri);
        HttpGet httpget = new HttpGet(uri);
        httpget.addHeader("Cookie", cookie);
        httpget.addHeader("Host", "xueqiu.com");
        httpget.addHeader("RA-Sid", "3CB01D6F-20140709-112055-4c09cf-c1239f");
        LogUtil.info("Executing request " + httpget.getRequestLine());
        CloseableHttpClient httpclient = HttpClients.custom().build();
        CloseableHttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        String s = EntityUtils.toString(entity);
        response.close();
        httpclient.close();
        JSONObject obj = JSON.parseObject(s);
        JSONArray chartlist = (JSONArray) obj.get("chartlist");
        List<StockPrice> stockPrices = Lists.newArrayList();
        if (chartlist == null) {
            LogUtil.info(LogUtil.net, "抓取雪球网数据失败,url:" + url.toString() + ",result is:" + s);
        }
        for (int i = 0; chartlist != null && i < chartlist.size(); i++) {
            StockPrice stockPrice = new StockPrice();
            JSONObject jsonObject = chartlist.getJSONObject(i);
            JSONObject preObject = null;
            JSONObject nextObject = null;
            if (i > 0) {
                preObject = chartlist.getJSONObject(i - 1);
            }
            if (i < chartlist.size() - 1) {
                nextObject = chartlist.getJSONObject(i + 1);
            }
            stockPrice.chg = Double.parseDouble(jsonObject.get("chg").toString());
            stockPrice.close = Double.parseDouble(jsonObject.get("close").toString());
            stockPrice.dea = Double.parseDouble(jsonObject.get("dea").toString());
            stockPrice.dif = Double.parseDouble(jsonObject.get("dif").toString());
            stockPrice.high = Double.parseDouble(jsonObject.get("high").toString());
            stockPrice.low = Double.parseDouble(jsonObject.get("low").toString());
            stockPrice.ma10 = Double.parseDouble(jsonObject.get("ma10").toString());
            stockPrice.ma20 = Double.parseDouble(jsonObject.get("ma20").toString());
            stockPrice.ma30 = Double.parseDouble(jsonObject.get("ma30").toString());
            stockPrice.ma5 = Double.parseDouble(jsonObject.get("ma5").toString());
            stockPrice.macd = Double.parseDouble(jsonObject.get("macd").toString());
            stockPrice.open = Double.parseDouble(jsonObject.get("open").toString());
            stockPrice.percent = Double.parseDouble(jsonObject.get("chg").toString());
            stockPrice.volume = new BigDecimal(jsonObject.get("volume").toString()).longValue();
            stockPrice.stock = stock;
            stockPrice.turnrate = Double.parseDouble(jsonObject.get("turnrate").toString());
            stockPrice.thedate = dateFormat_US.parse(jsonObject.get("time").toString());
            stockPrice.preDate = (i == 0 && begin.getTime() != Constants.date20100101.getTime()) ? begin : preObject != null ? dateFormat_US.parse(preObject.get("time").toString()) : null;
            stockPrice.nextDate = nextObject != null ? dateFormat_US.parse(nextObject.get("time").toString()) : null;
            stockPrices.add(stockPrice);
        }
        return stockPrices;
    }

    private String getPeroidParam(Period period) {
        switch (period) {
            case day:
                return "1day";
            case week:
                return "1week";
            case month:
                return "1month";
            default:
                return "1day";
        }
    }

}
