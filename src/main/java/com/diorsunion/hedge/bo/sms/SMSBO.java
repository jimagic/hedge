package com.diorsunion.hedge.bo.sms;

import com.diorsunion.hedge.common.ServiceResult;
import com.diorsunion.hedge.domain.SMS;

import java.util.Map;
import java.util.Set;

/**
 * 短消息BO
 * Created by wanglaoshi on 2016/4/23.
 */
public interface SMSBO {
    /**
     * 短信发送批量接口
     * @param sms 短信实体
     * @return
     */
    ServiceResult sendMessage(SMS sms);

}
