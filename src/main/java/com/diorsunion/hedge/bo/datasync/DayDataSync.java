package com.diorsunion.hedge.bo.datasync;

import com.diorsunion.hedge.util.CalendarUtils;
import com.diorsunion.hedge.common.Constants;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by harley-dog<custer7572@163.com> on 2015/12/12.
 */
@Component("DayDataSync")
public class DayDataSync extends StockDataSync {
    @Override
    public Period getPeriod() {
        return Period.day;
    }


    @Override
    public Date getBegin(Date date, Stock stock) {
        StockPrice query = new StockPrice();
        query.stock = stock;
        query.thedate = date;
        Date maxDate = stockPriceRepository.getMaxDate(stock, date, getPeriod());
        return maxDate == null ? Constants.date20100101 : CalendarUtils.addDate(maxDate, 1);
    }

    @Override
    public Date getEnd(Date date, Stock stock) {
        return CalendarUtils.addDate(date, 0);
    }
}
