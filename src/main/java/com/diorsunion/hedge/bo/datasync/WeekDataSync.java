package com.diorsunion.hedge.bo.datasync;

import com.diorsunion.hedge.common.Constants;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by harley-dog<custer7572@163.com> on 2015/12/12.
 */
@Component("WeekDataSync")
public class WeekDataSync extends StockDataSync {
    @Override
    protected Period getPeriod() {
        return Period.week;
    }

    @Override
    public Date getBegin(Date date, Stock stock) {
        StockPrice query = new StockPrice();
        query.stock = stock;
        query.thedate = date;
        Date maxDate = stockPriceRepository.getMaxDate(stock, date, getPeriod());
        if (maxDate == null) {
            return Constants.date20100101;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(maxDate);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.WEEK_OF_YEAR, 1);
        c.set(Calendar.DAY_OF_WEEK, 1);
        return c.getTime();
    }

    @Override
    public Date getEnd(Date date, Stock stock) {
        //上周最后一天
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
//        c.add(Calendar.WEEK_OF_YEAR, -1);
        c.set(Calendar.DAY_OF_WEEK, 1);
        c.add(Calendar.DATE, -1);
        return c.getTime();
    }
}
