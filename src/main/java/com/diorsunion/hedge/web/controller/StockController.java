package com.diorsunion.hedge.web.controller;

import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.repository.StockRepository;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author harley-dog on 2014/7/7.
 */
@Controller
@Scope("prototype")
@RequestMapping("/stock")
public class StockController {
    @Resource
    StockRepository stockRepository;

    @RequestMapping(value = "", method = {RequestMethod.GET})
    @ResponseBody
    public JsonResult<Stock> query() {
        List<Stock> stocks = stockRepository.findAll(null, null);
        return JsonResult.succResult(stocks);
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    @ResponseBody
    public JsonResult<Stock> get(@PathVariable String code) {
        Stock stock = stockRepository.get(code);
        return JsonResult.succResult(stock);
    }
}
