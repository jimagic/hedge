package com.diorsunion.hedge.web.controller;

import com.diorsunion.hedge.task.StockPriceSyncTask;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 人工调度Task,不用等系统执行
 * Created by dingshanyyj on 15/12/24.
 */

@Controller
@Scope("prototype")
@RequestMapping("/task/stock-price")
public class StockPriceSyncTaskSchedule {

    @Resource
    StockPriceSyncTask stockPriceSyncTask;

    @RequestMapping(value = "/day")
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public JsonResult day() {
        stockPriceSyncTask.day();
        return JsonResult.succResult("done");
    }

    @RequestMapping(value = "/week")
    @ResponseBody
    public JsonResult week() {
        stockPriceSyncTask.week();
        return JsonResult.succResult("done");
    }

    @RequestMapping(value = "/month")
    @ResponseBody
    public JsonResult month() {
        stockPriceSyncTask.month();
        return JsonResult.succResult("done");
    }


}
