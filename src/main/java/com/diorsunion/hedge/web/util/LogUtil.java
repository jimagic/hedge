package com.diorsunion.hedge.web.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by harley-dog<custer7572@163.com> on 2015/12/12.
 */
public class LogUtil {
    public final static Logger net = LoggerFactory.getLogger("net");
    public final static Logger app = LoggerFactory.getLogger("app");
    public final static Logger task = LoggerFactory.getLogger("task");
    public final static Logger debug = LoggerFactory.getLogger("debug");//生产生该日志全部不输出
    public final static Logger error = LoggerFactory.getLogger("error");
    public static void info(String message){
        app.info(message);
    }

    //生产上该方法的日志全部不输出
    public static void debug(String message){
        debug.debug(message);
    }
    public static void error(String message,Throwable e){
        error.error(message, e);
    }

    public static void info(Logger logger, String message) {
        logger.info(message);
    }

    public static void debug(Logger logger, String message) {
        logger.debug(message);
    }

    public static void error(Logger logger, String message, Throwable e) {
        logger.error(message, e);
    }

}
