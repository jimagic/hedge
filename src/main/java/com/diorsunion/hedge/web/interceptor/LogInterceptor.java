package com.diorsunion.hedge.web.interceptor;

import com.diorsunion.hedge.web.util.LogUtil;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by dingshanyyj on 15/12/24.
 */
//@Component
public class LogInterceptor implements HandlerExceptionResolver {

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        LogUtil.error(request.getRequestURI() + " error", ex);
        ModelAndView modelAndView = new ModelAndView("errorPage");
        return modelAndView;
    }
}
