package com.diorsunion.hedge.task;

import com.diorsunion.hedge.bo.datasync.StockDataSync;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.repository.StockRepository;
import com.diorsunion.hedge.web.util.LogUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by harley-dog<custer7572@163.com> on 2015/12/12.
 */
@Component
public class StockPriceSyncTask {
    @Resource
    @Qualifier("DayDataSync")
    StockDataSync dayDataSync;

    @Resource
    @Qualifier("WeekDataSync")
    StockDataSync weekDataSync;

    @Resource
    @Qualifier("MonthDataSync")
    StockDataSync monthDataSync;

    @Resource
    StockRepository stockRepository;

    //每天7点执行
    @Scheduled(cron = "0 0 7 * * ?")
    public void day() {
        run(dayDataSync);
    }

    //每周星期六的8点执行
    @Scheduled(cron = "0 0 8 * * SUN")
    public void week() {
        run(weekDataSync);
    }

    //每月1号的9点执行
    @Scheduled(cron = "0 0 9 1 * ?")
    public void month() {
        run(monthDataSync);
    }

    public void run(StockDataSync stockDataSync) {
        Date date = new Date();
        List<Stock> stockSet = stockRepository.findAll(null, null);
        stockSet.stream().forEach(stock -> {
            try {
                stockDataSync.sync(date, stock);
            } catch (Exception e) {
                LogUtil.error(stock.code + " sync error", e);
            }
        });
    }

    /**
     * 同步指定的股票数据
     * @param stockDataSync
     * @param code
     */
    public void run(StockDataSync stockDataSync, String code) {
        Date date = new Date();
        Stock stock = stockRepository.get(code);
        try {
            if(stock!=null){
                stockDataSync.sync(date, stock);
            }
        } catch (Exception e) {
            LogUtil.error(stock.code + " sync error", e);
        }
    }
}
