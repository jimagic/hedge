package com.diorsunion.hedge.domain;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class MACD {
    public final MACD.Type type;//MACD特征类型
    public final int level;    //特征级别

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MACD macd = (MACD) o;

        if (level != macd.level) return false;
        return type == macd.type;

    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + level;
        return result;
    }

    public MACD(MACD.Type type, int level) {
        this.type = type;
        this.level = level;
    }

    @Override
    public String toString() {
        return "MCAD{" +
                "type=" + type +
                ", level=" + level +
                '}';
    }

    public enum  Type {
        JINCHA,//金叉
        SICHA,  //死叉
        buttom_divergence,//底背离
        top_divergence;//顶背离
    }
}
