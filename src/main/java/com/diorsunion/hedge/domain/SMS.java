package com.diorsunion.hedge.domain;

/**
 * Created by custe on 2016/4/24.
 */

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * 短信实体
 */
public class SMS {
    public Collection<String> recTelSet;    //接受者手机号集合
    public String templateCode;     //短信模版码
    public String signName;           //短信签名
    public Map<String,String> params;//变量替换

    public SMS(Collection<String> recTelSet, String templateCode, String signName, Map<String, String> params) {
        this.recTelSet = recTelSet;
        this.templateCode = templateCode;
        this.signName = signName;
        this.params = params;
    }
}
