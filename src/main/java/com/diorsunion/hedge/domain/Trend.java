package com.diorsunion.hedge.domain;

/**
 * Created by wanglaoshi on 2016/4/17.
 */
public enum  Trend {
    UP,DOWN,EVEN;
    public static final Trend judge(double ex,double cu){
        return ex<cu?UP:(ex>cu)?DOWN:EVEN;
    }
}
