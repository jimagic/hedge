package com.diorsunion.hedge.base;

import com.diorsunion.dbtest.spring.DBTestClassRunner;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 连接外部API接口的测试基类
 * 需要网络环境
 * Created by custe on 2016/4/24.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Category(ExternalApiBaseTest.class)
@ContextConfiguration(locations = {
        "classpath:springbeans-config-test.xml",
        "classpath:springbeans-api-test.xml"
})
public class ExternalApiBaseTest extends BaseTest{
    @Override
    protected String getBaseName() {
        return "外部API接口集成测试";
    }

    @Rule
    public TestName name = new TestName();

    @Override
    protected TestName getTestName() {
        return name;
    }
}
