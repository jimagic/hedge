package com.diorsunion.hedge.base;

import com.diorsunion.dbtest.spring.DBTestClassRunner;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

/**
 * @author harley-dog on 2015/4/9.
 */

@RunWith(DBTestClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:springbeans-ds-embedded-test.xml",
        "classpath:springbeans-mybatis.xml",
        "classpath:springbeans-config.xml",
        "classpath:springbeans-bo.xml"
})
@Category(EmbeddedBOBaseTest.class)
public abstract class EmbeddedBOBaseTest extends BaseTest{

    @Rule
    public TestName name = new TestName();

    @Override
    protected TestName getTestName() {
        return name;
    }

    @Override
    protected String getBaseName() {
        return "业务层内存数据库单元测试";
    }
}
