package com.diorsunion.hedge.base;

import org.junit.Before;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;

/**
 * 普通单元测试基类
 *
 * Created by wanglaoshi on 2016/4/24.
 */
@Category(UnitBaseTest.class)
public class UnitBaseTest extends BaseTest{
    @Rule
    public TestName name = new TestName();

    @Override
    protected TestName getTestName() {
        return name;
    }
    @Override
    protected String getBaseName() {
        return "普通单元测试";
    }


}
