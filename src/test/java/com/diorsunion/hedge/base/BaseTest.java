package com.diorsunion.hedge.base;

import org.junit.Before;
import org.junit.rules.TestName;

/**
 * Created by custe on 2016/4/24.
 */
public abstract class BaseTest {
    @Before
    public void callMyName(){
        System.out.println(getBaseName()+":"+getClass().getName()+"."+getTestName().getMethodName()+"()");
    }

    protected abstract String getBaseName();
    protected abstract TestName getTestName();
}
