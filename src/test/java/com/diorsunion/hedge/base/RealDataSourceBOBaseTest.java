package com.diorsunion.hedge.base;

import com.diorsunion.dbtest.spring.DBTestClassRunner;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

/**
 * 连接真实数据库的测试基类
 * Created by wanglaoshi on 2016/4/16.
 */
@RunWith(DBTestClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:springbeans-ds-mysql-test.xml",
        "classpath:springbeans-mybatis.xml",
        "classpath:springbeans-config.xml",
        "classpath:springbeans-bo.xml"
})
@Category(RealDataSourceBOBaseTest.class)
public abstract class RealDataSourceBOBaseTest extends BaseTest{

    @Test
    public void test(){
    }

    @Rule
    public TestName name = new TestName();

    @Override
    protected TestName getTestName() {
        return name;
    }

    @Override
    protected String getBaseName() {
        return "实体数据库集成测试";
    }
}
