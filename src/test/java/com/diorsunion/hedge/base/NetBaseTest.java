package com.diorsunion.hedge.base;

import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;

/**
 * 连接外部网络接口 的测试基类
 * 需要网络环境
 * Created by custe on 2016/4/24.
 */
@Category(NetBaseTest.class)
public class NetBaseTest extends BaseTest{
    @Override
    protected String getBaseName() {
        return "网络调用集成测试";
    }

    @Rule
    public TestName name = new TestName();

    @Override
    protected TestName getTestName() {
        return name;
    }
}
