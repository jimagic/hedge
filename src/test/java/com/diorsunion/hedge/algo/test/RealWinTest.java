package com.diorsunion.hedge.algo.test;

import com.diorsunion.hedge.algo.*;
import com.diorsunion.hedge.algo.Operation;
import com.diorsunion.hedge.base.RealDataSourceBOBaseTest;
import com.diorsunion.hedge.bo.stockdatainit.StockPriceInit;
import com.diorsunion.hedge.util.CalendarUtils;
import com.diorsunion.hedge.dal.entity.Account;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 初始化股票数据
 *
 * @author harley-dog on 2015/6/4.
 */
public class RealWinTest extends RealDataSourceBOBaseTest{

    final static double init_money = 100000;
    static List<Operation> opers = new ArrayList<>();
    static Date begin;
    static Date end;
    static Stock stock_0 = new Stock();
    static Stock stock_1 = new Stock();
    @Resource @Qualifier("RealStockPriceInit")
    StockPriceInit stockPriceInit;

    static {
//        opers.add(new Oper1(null));
//        opers.add(new Oper2(null));
//        opers.add(new Oper3(ImmutableMap.of(Operation.PROFIT, 1, Operation.LOSS, -1)));
//        opers.add(new Oper4(ImmutableMap.of(Operation.N, 2)));
//        opers.add(new Oper5(ImmutableMap.of(Operation.PROFIT, 1, Operation.LOSS, -1)));
//        opers.add(new Oper6(ImmutableMap.of(Operation.PROFIT, 1, Operation.LOSS, -1)));
//        opers.add(new Oper7(ImmutableMap.of(Operation.PROFIT, 1, Operation.LOSS, -1)));
        opers.add(new Oper8(ImmutableMap.of(Operation.PROFIT, 1, Operation.LOSS, -1)));
        stock_0.code = "UVXY";
        stock_1.code = "SVXY";
        try {
            begin = CalendarUtils.parseSimple("2016-01-01");
            end = CalendarUtils.parseSimple("2016-03-24");
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test() {

        //初始化股票价格
        List<Date> dates = stockPriceInit.init(begin, end, stock_0, stock_1);
        for (Operation oper : opers) {
            System.out.println("----------------------------------------------------------------");

            List<Account> account_per_days = Lists.newArrayList();//每天的账户情况
            Account account = new Account(dates.get(0), init_money, stock_0, stock_1);//初始化一个账户
            account_per_days.add(account);
            int day = 1;
            System.out.println("第" + (day++) + "天,初始化,启动资金" + init_money);
            oper.oper(account, account_per_days);
            System.out.println();
            dates.remove(0);
            for (Date date : dates) {
                Account nextAccount = account.initNextDayAccount(date);
                account_per_days.add(nextAccount);
                RandomWinTest.printStockDetail(day, date, nextAccount, "操作前");
                oper.oper(nextAccount, account_per_days);
                RandomWinTest.printStockDetail(day++, date, nextAccount, "操作后");
                System.out.println();
                account = nextAccount;
            }
            account.close(StockPrice.PriceType.CLOSE);//最后一天，全部平仓
            double lastValue = account.getTotalValue(StockPrice.PriceType.CLOSE);
            System.out.println("操作方法:" + oper.getDesc());
            int days = CalendarUtils.getDiff(begin, end, Calendar.DATE);
            System.out.println("操作" + days + "天,最终收益:" + String.format("%.2f", (lastValue - init_money) * 100 / init_money) + "%");
        }
        System.out.println("*--------------------------------------------------------------*");
    }


}
