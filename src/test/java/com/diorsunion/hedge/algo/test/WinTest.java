package com.diorsunion.hedge.algo.test;

import com.diorsunion.hedge.algo.Operation;
import com.diorsunion.hedge.base.RealDataSourceBOBaseTest;
import com.diorsunion.hedge.dal.entity.Account;
import com.diorsunion.hedge.dal.entity.StockPrice;
import com.diorsunion.hedge.dal.entity.TradeLog;

import java.util.Date;
import java.util.List;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class WinTest extends RealDataSourceBOBaseTest {
    public static double dayoper(List<Date> dates, Account account, List<Account> account_per_days, Operation operation){
        int day = 2;
        for (Date date : dates) {
            Account nextAccount = account.initNextDayAccount(date);
            account_per_days.add(nextAccount);
//            RandomWinTest.printStockDetail(day, date, nextAccount, "操作前");
            operation.oper(nextAccount, account_per_days);
//            RandomWinTest.printStockDetail(day, date, nextAccount, "操作后");
//            System.out.println();
            account = nextAccount;
            day++;
        }
        List<TradeLog> tradeLogs = account.close(StockPrice.PriceType.CLOSE);//最后一天，全部平仓
        operation.tradeLogs.addAll(tradeLogs);
        double lastValue = account.getTotalValue(StockPrice.PriceType.CLOSE);
        return lastValue;
    }
}
