package com.diorsunion.hedge.bo.db.test;

import com.diorsunion.dbtest.spring.DBTestClassRunner;
import com.diorsunion.hedge.base.RealDataSourceBOBaseTest;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.repository.StockPriceRepository;
import com.diorsunion.hedge.dal.repository.StockRepository;
import com.google.common.collect.Lists;
import org.junit.runner.RunWith;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author harley-dog on 2015/7/17.
 */
@RunWith(DBTestClassRunner.class)
public class YinnYang extends RealDataSourceBOBaseTest{

    public final static SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    final static List<Integer> stockIds = Lists.newArrayList(6, 7);
    public static Date begin;
    public static Date end;

    static {
        try {
            begin = s.parse("2015-01-02");
            end = s.parse("2015-01-02");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Resource
    StockPriceRepository stockPriceRepository;
    @Resource
    StockRepository stockRepository;

    @org.junit.Test
    public void test() {
        Stock s1 = stockRepository.get("YANG");
        Stock s2 = stockRepository.get("YINN");
        assert (s1!=null);
        assert (s2!=null);
//        List<> stockPriceRepository.findStockPrice(s1,begin,end,"1day");
    }
}
