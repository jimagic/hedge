package com.diorsunion.hedge.bo.db.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.hedge.base.EmbeddedBOBaseTest;
import com.diorsunion.hedge.bo.db.StockPriceBO;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import com.diorsunion.hedge.dal.repository.StockPriceRepository;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by dingshanyyj on 15/12/24.
 */
public class StockPriceBOTest extends EmbeddedBOBaseTest {

    @Resource
    protected StockPriceRepository stockPriceRepository;
    @Resource
    StockPriceBO stockPriceBO;
    Stock stock = new Stock();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void before() throws NoSuchFieldException, IllegalAccessException {
        stock.code = "BABA";
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 3, tableName = "stock_price_day", custom = "id={1,2,3};stock_code=BABA;thedate={2015-11-29,2015-11-30,2015-12-01}")
    })
    public void testStoreToDB() throws Exception {
        Date thedate = simpleDateFormat.parse("2015-12-01");
        StockPrice stockPrice = new StockPrice();
        stockPrice.thedate = thedate;
        stockPriceBO.insert(stock, stockPrice, Period.day);

        Date begin = simpleDateFormat.parse("2015-11-29");
        Date end = simpleDateFormat.parse("2015-12-01");
        List<StockPrice> stockPrices = stockPriceRepository.findStockPrice(stock, begin, end, Period.day);
        assert (stockPrices.size() == 3);
    }
}
