package com.diorsunion.hedge.bo.stockdatainit.test;

import com.diorsunion.hedge.base.RealDataSourceBOBaseTest;
import com.diorsunion.hedge.bo.datasync.StockDataSync;
import com.diorsunion.hedge.dal.repository.StockRepository;
import com.diorsunion.hedge.task.StockPriceSyncTask;
import com.diorsunion.hedge.util.ReflectionUtil;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;

/**
 * 初始化股票数据
 *
 * @author harley-dog on 2015/6/4.
 */
public class DayStockPriceDataInitRepositoryTest extends RealDataSourceBOBaseTest {

    static StockPriceSyncTask stockPriceSyncTask = new StockPriceSyncTask();
    @Resource @Qualifier("DayDataSync")
    StockDataSync dayDataSync;
    @Resource @Qualifier("WeekDataSync")
    StockDataSync weekDataSync;
    @Resource @Qualifier("MonthDataSync")
    StockDataSync monthDataSync;
    @Resource
    StockRepository stockRepository;

    @BeforeClass
    public void beforeClass() throws IllegalAccessException {
        ReflectionUtil.setValue(stockPriceSyncTask, "stockRepository",stockRepository);
    }

    @Test
    @Ignore
    public void test() {
        stockPriceSyncTask.run(dayDataSync,"UVXY");
        stockPriceSyncTask.run(dayDataSync,"SVXY");
        stockPriceSyncTask.run(dayDataSync,"UWTI");
        stockPriceSyncTask.run(dayDataSync,"DWTI");
        stockPriceSyncTask.run(dayDataSync,"DUST");
        stockPriceSyncTask.run(dayDataSync,"NUGT");
        stockPriceSyncTask.run(dayDataSync,"YANG");
        stockPriceSyncTask.run(dayDataSync,"YINN");
        stockPriceSyncTask.run(dayDataSync,"SQQQ");
        stockPriceSyncTask.run(dayDataSync,"TQQQ");
        stockPriceSyncTask.run(dayDataSync,"SPXU");
        stockPriceSyncTask.run(dayDataSync,"UPRO");
        stockPriceSyncTask.run(dayDataSync,"UDOW");
        stockPriceSyncTask.run(dayDataSync,"SDOW");
    }
}
