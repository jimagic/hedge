package com.diorsunion.hedge.bo.quota.test;

import com.diorsunion.hedge.base.RealDataSourceBOBaseTest;
import com.diorsunion.hedge.util.CalendarUtils;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import com.diorsunion.hedge.dal.repository.StockPriceRepository;
import com.diorsunion.hedge.bo.quota.MACDFilter;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by wanglaoshi on 2016/4/16.
 */
public class MACDFilterTest extends RealDataSourceBOBaseTest {
    @Resource
    StockPriceRepository stockPriceRepository;
    MACDFilter macdFilter = new MACDFilter();

    @Test
    public void mutliStockMacd() throws ParseException {
        Stock stock = new Stock();
        stock.code = "UVXY";
        Date begin = CalendarUtils.parseSimple("2015-01-20");
        Date end = CalendarUtils.parseSimple("2016-04-30");
        List<StockPrice> stockPriceList = stockPriceRepository.findStockPrice(stock, begin, end, Period.day);
        macdFilter.filter(stockPriceList);
        stockPriceList.stream()
                .filter(x -> !x.macds.isEmpty())
                .forEach(x -> System.out.println(stock.code + ":" + CalendarUtils.formatSimple(x.thedate) + ":" + x.macds));
    }

    @Test
    public void testCustomMacd() throws ParseException {
        Stock stock = new Stock();
        stock.code = "UVXY";
        Date begin = CalendarUtils.parseSimple("2015-01-20");
        Date end = CalendarUtils.parseSimple("2016-04-30");
        List<StockPrice> stockPriceList = stockPriceRepository.findStockPrice(stock, begin, end, Period.day);
        macdFilter.filter(stockPriceList);
        stockPriceList.stream()
                .forEach(x -> {
                    System.out.println(stock.code + ":" + CalendarUtils.formatSimple(x.thedate) + ",dif:" + f(x.dif) + ",dea:"+ f(x.dea)+",macd:"+ f(x.macd)+",close:"+f(x.close));
                    System.out.println(stock.code + ":" + CalendarUtils.formatSimple(x.thedate) + ",dif:" + f(x.ex.dif) + ",dea:"+ f(x.ex.dea) +",macd:"+f(x.ex.macd)+",price:"+f(x.ex.price)+",macdtrend:"+x.ex.macdTrend);
                    System.out.println();
                });
    }

    private String f(double x){
        return String.format("%.2f",x);
    }

    @Test
    public void testCalcDifDeaMacdPriceByPrice(){
        double s_ema12 = 1d;
        double s_ema26 = 1.5d;
        double s_dea = 2d;
        double close = 10;

        double dif = MACDFilter.calcDifByPrice(s_ema12,s_ema26,close);
        System.out.println("dif="+dif);
        double dea = MACDFilter.calcDeaByPrice(s_ema12,s_ema26,s_dea,close);
        System.out.println("dea="+dea);
        double macd = MACDFilter.calcMacdByPrice(s_ema12,s_ema26,s_dea,close);
        System.out.println("macd="+macd);
        double price = MACDFilter.calcPriceByMacd(s_ema12,s_ema26,s_dea,macd);
        System.out.println("price="+price);
        assert (new BigDecimal(close).setScale(2,BigDecimal.ROUND_HALF_UP).equals(new BigDecimal(price).setScale(2,BigDecimal.ROUND_UP)));
    }
}
