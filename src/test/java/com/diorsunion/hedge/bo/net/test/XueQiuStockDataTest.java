package com.diorsunion.hedge.bo.net.test;

import com.alibaba.fastjson.JSON;
import com.diorsunion.hedge.base.NetBaseTest;
import org.apache.http.HttpEntity;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

/**
 * @author harley-dog on 2015/7/12.
 */
public class XueQiuStockDataTest  extends NetBaseTest{

        //        http://xueqiu.com/stock/forchartk/stocklist.json?symbol=BABA&period=1day&type=normal&begin=1405166656784&end=1436702656784&_=1436702656784
    public static void main(String[] args) throws Exception {

        URL url = new URL("http://xueqiu.com");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();
        String cookie = connection.getHeaderField("Set-Cookie");
        URI uri = new URIBuilder()
                .setScheme("http")
                .setHost("xueqiu.com")
                .setPath("/stock/forchartk/stocklist.json")
                .setParameter("symbol", "BABA")
                .setParameter("period", "1day")
                .setParameter("type", "normal")
                .setParameter("begin", "1405166656784")
                .setParameter("end", "")
                .setParameter("_", String.valueOf(System.currentTimeMillis()))
                .build();
        HttpGet httpget = new HttpGet(uri);
        System.out.println(httpget.getURI());
        System.out.println("Executing request " + httpget.getRequestLine());
        httpget.addHeader("Cookie", cookie);
        httpget.addHeader("Host", "xueqiu.com");
        httpget.addHeader("RA-Sid", "3CB01D6F-20140709-112055-4c09cf-c1239f");

        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie clientCookie = new BasicClientCookie("Host", "xueqiu.com");
        clientCookie.setDomain("xueqiu.com");
        clientCookie.setPath("/");
        cookieStore.addCookie(clientCookie);
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .build();
        CloseableHttpResponse response1 = httpclient.execute(httpget);
        try {
            System.out.println(response1.getStatusLine());
            HttpEntity entity = response1.getEntity();
            String s = EntityUtils.toString(entity);
            System.out.println(s);
            JSON.parse(s);
            EntityUtils.consume(entity);
        } finally {
            response1.close();
        }
    }
}
