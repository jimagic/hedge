package com.diorsunion.hedge.bo.net.test;

import com.diorsunion.hedge.base.NetBaseTest;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by harley-dog<custer7572@163.com> on 2015/12/11.
 */
public class CookieTest extends NetBaseTest {
    @Test
    public void test() throws Exception {
        URL url = new URL("http://xueqiu.com");
        HttpGet httpget = new HttpGet(url.toURI());
        CloseableHttpClient httpclient = HttpClients.custom().build();
        CloseableHttpResponse response = httpclient.execute(httpget);
        Header[] headers = response.getAllHeaders();
        StringBuilder cookie = new StringBuilder();
        for (Header header : headers) {
            if ("Set-Cookie".equals(header.getName())) {
                cookie.append(header.getValue()).append(";");
            }
        }
        assert (cookie != null);
        System.out.println(cookie);
    }

}
