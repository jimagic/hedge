package com.diorsunion.hedge.bo.net.test;

import com.diorsunion.hedge.base.NetBaseTest;
import com.diorsunion.hedge.bo.net.DataFetcher;
import com.diorsunion.hedge.bo.net.XueQiuDataFetcher;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by dingshanyyj on 15/12/23.
 */
public class DataFetcherTest  extends NetBaseTest {

    @Test
    public void testCookie() throws SAXException, IOException, URISyntaxException {
        HttpGet httpget = new HttpGet(new URI("http://xueqiu.com"));
        CloseableHttpClient httpclient = HttpClients.custom().build();
        CloseableHttpResponse response = httpclient.execute(httpget);
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            System.out.println(header);
        }
    }

    @Test
    public void testGetCookie() throws URISyntaxException, IOException {
        String url = "http://xueqiu.com";
        DataFetcher dataFetcher = new XueQiuDataFetcher();
        String cookie = dataFetcher.getCookie(new URI(url));
        System.out.println(cookie);
        assert (cookie.contains("xq_r_token="));
        assert (cookie.contains("xq_a_token="));
    }
}
