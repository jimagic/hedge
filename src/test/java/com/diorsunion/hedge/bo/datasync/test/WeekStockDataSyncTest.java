package com.diorsunion.hedge.bo.datasync.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.hedge.base.EmbeddedBOBaseTest;
import com.diorsunion.hedge.bo.net.DataFetcher;
import com.diorsunion.hedge.bo.datasync.WeekDataSync;
import com.diorsunion.hedge.bo.db.StockPriceBO;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.util.ReflectionUtil;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import com.diorsunion.hedge.dal.repository.StockPriceRepository;
import com.diorsunion.hedge.dal.repository.StockRepository;
import com.google.common.collect.Lists;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by harley-dog<custer7572@163.com> on 15/12/16.
 */
public class WeekStockDataSyncTest extends EmbeddedBOBaseTest {
    @Resource
    protected StockPriceRepository stockPriceRepository;
    @Resource
    protected StockRepository stockRepository;
    @Resource
    protected StockPriceBO stockPriceBO;
    WeekDataSync weekDataSync = new WeekDataSync();
    DataFetcher dataFetcher = EasyMock.createMock(DataFetcher.class);
    Stock stock = new Stock();
    Date today;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat fullDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");

    @Before
    public void before() throws NoSuchFieldException, IllegalAccessException, ParseException {
        ReflectionUtil.setValue(weekDataSync, "dataFetcher", dataFetcher);
        ReflectionUtil.setValue(weekDataSync, "stockPriceRepository", stockPriceRepository);
        ReflectionUtil.setValue(weekDataSync, "stockRepository", stockRepository);
        ReflectionUtil.setValue(weekDataSync, "stockPriceBO", stockPriceBO);
        stock.code = "BABA";
        today = simpleDateFormat.parse("2015-12-28");
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_week", custom = "id=1;stock_code=BABA;thedate=2015-12-19")
    })
    public void testGetBegin() throws ParseException {
        Date begin = weekDataSync.getBegin(simpleDateFormat.parse("2015-12-28"), stock);
        Date expect = simpleDateFormat.parse("2015-12-20");
        assert (begin.equals(expect));
    }

    @Test
    public void testGetEnd() throws ParseException {
        Date end = weekDataSync.getEnd(simpleDateFormat.parse("2015-12-28"),stock);
        Date expect = fullDateFormat.parse("2015-12-26 23:59:59 999");
        assert (end.equals(expect));
        end = weekDataSync.getEnd(simpleDateFormat.parse("2015-12-27"),stock);
        expect = fullDateFormat.parse("2015-12-26 23:59:59 999");
        assert (end.equals(expect));
        end = weekDataSync.getEnd(simpleDateFormat.parse("2016-01-01"),stock);
        expect = fullDateFormat.parse("2015-12-26 23:59:59 999");
        assert (end.equals(expect));
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_week", custom = "id=1;stock_code=BABA;thedate=2015-12-07")
    })
    public void testSync() throws Exception {
        Date begin = simpleDateFormat.parse("2015-12-13");
        Date end = weekDataSync.getEnd(today, stock);
        List<StockPrice> list = getStockPrice();
        EasyMock.expect(dataFetcher.fetchStockPrice(stock, begin, end, Period.week)).andReturn(list);
        EasyMock.replay(dataFetcher);
        weekDataSync.sync(today, stock);

        List<StockPrice> stockPrices = stockPriceRepository.findStockPrice(stock, simpleDateFormat.parse("2015-12-07"), end, Period.week);
        assert (stockPrices.size() == 3);
        for (int i = 0; i < stockPrices.size(); i++) {
            StockPrice x = stockPrices.get(i);
            assert (x != null);
            assert (x.thedate != null);
            if (i > 0) {
                assert (x.preDate.getTime() == stockPrices.get(i - 1).thedate.getTime());
            }
            if (i < stockPrices.size() - 1) {
                assert (x.nextDate.getTime() == stockPrices.get(i + 1).thedate.getTime());
            }
        }
    }

    public List<StockPrice> getStockPrice() throws ParseException {
        StockPrice s1 = new StockPrice();
        StockPrice s2 = new StockPrice();
        s1.thedate = simpleDateFormat.parse("2015-12-14");
        s2.thedate = simpleDateFormat.parse("2015-12-21");
        return Lists.newArrayList(s1, s2);
    }
}
