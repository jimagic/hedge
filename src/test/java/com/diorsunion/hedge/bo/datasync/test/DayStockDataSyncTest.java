package com.diorsunion.hedge.bo.datasync.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.hedge.base.EmbeddedBOBaseTest;
import com.diorsunion.hedge.bo.net.DataFetcher;
import com.diorsunion.hedge.bo.datasync.DayDataSync;
import com.diorsunion.hedge.bo.db.StockPriceBO;
import com.diorsunion.hedge.util.CalendarUtils;
import com.diorsunion.hedge.common.Period;
import com.diorsunion.hedge.util.ReflectionUtil;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import com.diorsunion.hedge.dal.repository.StockPriceRepository;
import com.diorsunion.hedge.dal.repository.StockRepository;
import com.google.common.collect.Lists;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by harley-dog<custer7572@163.com> on 15/12/16.
 */
public class DayStockDataSyncTest extends EmbeddedBOBaseTest {
    @Resource
    protected StockPriceRepository stockPriceRepository;
    @Resource
    protected StockRepository stockRepository;
    @Resource
    protected StockPriceBO stockPriceBO;
    DayDataSync stockDataSync = new DayDataSync();
    DataFetcher dataFetcher = EasyMock.createMock(DataFetcher.class);
    Stock stock = new Stock();
    Date today;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void before() throws NoSuchFieldException, IllegalAccessException, ParseException {
        ReflectionUtil.setValue(stockDataSync, "dataFetcher", dataFetcher);
        ReflectionUtil.setValue(stockDataSync, "stockPriceRepository", stockPriceRepository);
        ReflectionUtil.setValue(stockDataSync, "stockRepository", stockRepository);
        ReflectionUtil.setValue(stockDataSync, "stockPriceBO", stockPriceBO);
        stock.code = "SQQQ";
        today = simpleDateFormat.parse("2015-12-28");
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_day", custom = "id=1;stock_code=SQQQ;thedate=2015-12-25")
    })
    public void testGetBegin1() throws ParseException {
        Date begin = stockDataSync.getBegin(simpleDateFormat.parse("2015-12-28"), stock);
        Date expect = simpleDateFormat.parse("2015-12-26");
        assert (begin.equals(expect));
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_day", custom = "id=1;stock_code=SQQQ;thedate=2015-12-28")
    })
    public void testGetBegin2() throws ParseException {
        Date begin = stockDataSync.getBegin(simpleDateFormat.parse("2015-12-30"), stock);
        Date expect = simpleDateFormat.parse("2015-12-29");
        assert (begin.equals(expect));
    }

    @Test
    public void testGetEnd() throws ParseException {
        Date end = stockDataSync.getEnd(today,stock);
        Date expect = simpleDateFormat.parse("2015-12-28");
        assert (end.equals(expect));
    }

    @Test
    @DataSets({
            @DataSet(entityClass = StockPrice.class, number = 1, tableName = "stock_price_day", custom = "id=1;stock_code=SQQQ;thedate=2015-11-30")
    })
    public void testSync() throws Exception {
        Date begin = simpleDateFormat.parse("2015-12-01");
        Date end = CalendarUtils.addDate(today, 0);
        List<StockPrice> list = getStockPrice();
        EasyMock.expect(dataFetcher.fetchStockPrice(stock, begin, end, Period.day)).andReturn(list);
        EasyMock.replay(dataFetcher);
        stockDataSync.sync(today, stock);

        List<StockPrice> stockPrices = stockPriceRepository.findStockPrice(stock, simpleDateFormat.parse("2015-11-30"), end, Period.day);
        assert (stockPrices.size() == 11);
        for (int i = 0; i < stockPrices.size(); i++) {
            StockPrice x = stockPrices.get(i);
            assert (x != null);
            assert (x.thedate != null);
            if (i > 0) {
                assert (x.preDate.getTime() == stockPrices.get(i - 1).thedate.getTime());
            }
            if (i < stockPrices.size() - 1) {
                assert (x.nextDate.getTime() == stockPrices.get(i + 1).thedate.getTime());
            }
        }
    }

    public List<StockPrice> getStockPrice() throws ParseException {
        StockPrice s1 = new StockPrice();
        StockPrice s2 = new StockPrice();
        StockPrice s3 = new StockPrice();
        StockPrice s4 = new StockPrice();
        StockPrice s5 = new StockPrice();
        StockPrice s6 = new StockPrice();
        StockPrice s7 = new StockPrice();
        StockPrice s8 = new StockPrice();
        StockPrice s9 = new StockPrice();
        StockPrice s0 = new StockPrice();
        s1.thedate = simpleDateFormat.parse("2015-12-01");
        s2.thedate = simpleDateFormat.parse("2015-12-02");
        s3.thedate = simpleDateFormat.parse("2015-12-03");
        s4.thedate = simpleDateFormat.parse("2015-12-04");
        s5.thedate = simpleDateFormat.parse("2015-12-05");
        s6.thedate = simpleDateFormat.parse("2015-12-08");
        s7.thedate = simpleDateFormat.parse("2015-12-09");
        s8.thedate = simpleDateFormat.parse("2015-12-10");
        s9.thedate = simpleDateFormat.parse("2015-12-11");
        s0.thedate = simpleDateFormat.parse("2015-12-12");
        return Lists.newArrayList(s1, s2, s3, s4, s5, s6, s7, s8, s9, s0);
    }
}
