package com.diorsunion.hedge.bo.sms.test;

import com.diorsunion.hedge.base.ExternalApiBaseTest;
import com.diorsunion.hedge.bo.sms.AliDaYuSMSBO;
import com.diorsunion.hedge.bo.sms.SMSBO;
import com.diorsunion.hedge.common.ServiceResult;
import com.diorsunion.hedge.domain.SMS;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 阿里大鱼测试
 * Created by wanglaoshi on 2016/4/23.
 */
public class AliDaYuSMSBOTest extends ExternalApiBaseTest {

    @Resource
    AliDaYuSMSBO smsbo;

    @Value("#{config['sms.test.recno']}")
    String telno;
    String templateCode = "SMS_8175534";
    String signName = "活动验证";

    @Test
    public void testSendMessage(){
        Map<String,String> params = Maps.newHashMap();
        params.put("code","wanglaoshi");
        params.put("product","pruduct11");
        params.put("item","itemaa");
        List<String> telList = Splitter.on(',').splitToList(telno);
        SMS sms = new SMS(telList,templateCode,signName,params);
        ServiceResult result = smsbo.sendMessage(sms);
        assert (result.success);
    }
}
