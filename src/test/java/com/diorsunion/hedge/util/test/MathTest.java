package com.diorsunion.hedge.util.test;

import com.diorsunion.hedge.base.UnitBaseTest;
import org.junit.Test;

/**
 * Created by dingshan.yyj on 2016/3/29.
 */
public class MathTest extends UnitBaseTest {
    @Test
    public void test(){
        double a0 = 100;
        double a1 = 100;
        int index = 10;
        for(int i=0;i<10;i++){
            a0 = a0*1.1;
            a1 = a1*0.9;
            System.out.println("a0="+f(a0)+";a1="+f(a1)+";total="+f(a0+a1));
        }
        System.out.println("a0="+f(a0)+";a1="+f(a1)+";total="+f(a0+a1));
        for(int i=0;i<10;i++){
            a0 = a0*0.9;
            a1 = a1*1.1;
            System.out.println("a0="+f(a0)+";a1="+f(a1)+";total="+f(a0+a1));
        }
        System.out.println("a0="+f(a0)+";a1="+f(a1)+";total="+f(a0+a1));
    }

    private String f(double d){
        return String.format("%.2f",d);
    }
}
