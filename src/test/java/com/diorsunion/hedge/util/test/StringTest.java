package com.diorsunion.hedge.util.test;

import com.diorsunion.hedge.base.UnitBaseTest;
import com.google.common.base.Joiner;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Set;

/**
 * Created by custe on 2016/4/24.
 */
public class StringTest extends UnitBaseTest {
    @Test
    public void testSetToString(){
        Set<String> set = Sets.newHashSet("11","22");
        Joiner joiner = Joiner.on(',');
        String s = joiner.join(set);
        assert ("11,22".equals(s));
    }
}
