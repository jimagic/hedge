package com.diorsunion.hedge.task;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.hedge.base.EmbeddedBOBaseTest;
import com.diorsunion.hedge.bo.datasync.StockDataSync;
import com.diorsunion.hedge.util.ReflectionUtil;
import com.diorsunion.hedge.dal.entity.Stock;
import com.diorsunion.hedge.dal.entity.StockPrice;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by dingshanyyj on 15/12/23.
 */
public class StockPriceSyncTaskTest extends EmbeddedBOBaseTest {
    @Resource
    StockPriceSyncTask stockPriceSyncTask = EasyMock.createMock(StockPriceSyncTask.class);

    StockDataSync dayDataSync = EasyMock.createMock(StockDataSync.class);

    @Before
    public void before() throws IllegalAccessException {
        ReflectionUtil.setValue(stockPriceSyncTask, "dayDataSync", dayDataSync);
    }

    @Test

    @DataSets({
            @DataSet(entityClass = Stock.class, number = 2, tableName = "stock", custom = "code={BABA,JD};"),
            @DataSet(entityClass = StockPrice.class, number = 2, tableName = "stock_price_day", custom = "id={1,2};code={BABA,JD};thedate=2015-11-30")
    })
    public void test() throws Exception {
        Stock stock1 = new Stock();
        stock1.code = "BABA";
        Stock stock2 = new Stock();
        stock2.code = "JD";
        dayDataSync.sync(EasyMock.anyObject(Date.class), EasyMock.eq(stock1));
        EasyMock.expectLastCall().times(3);
        dayDataSync.sync(EasyMock.anyObject(Date.class), EasyMock.eq(stock2));
        EasyMock.expectLastCall().times(1);
        EasyMock.replay(dayDataSync);
        stockPriceSyncTask.run(dayDataSync);
    }
}
