﻿SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS  `etf`;
CREATE TABLE `etf` (
  `stock_code` varchar(200) NOT NULL DEFAULT '' COMMENT '股票名称，关联stock.code',
  `type` char(1) NOT NULL DEFAULT '' COMMENT 'L:long做多;S:short做空',
  `reverse_code` varchar(255) DEFAULT NULL COMMENT '反向的ETF股票code',
  `refer_code` varchar(255) DEFAULT NULL COMMENT '关联的指数code',
  PRIMARY KEY (`stock_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ETF表';

DROP TABLE IF EXISTS  `stock`;
CREATE TABLE `stock` (
  `code` varchar(200) NOT NULL COMMENT '股票代码',
  `name` varchar(200) NOT NULL COMMENT '股票名称',
  `exchange` varchar(200) NOT NULL COMMENT '所在交易所机构',
  `url` varchar(255) DEFAULT NULL COMMENT '爬虫抓取的页面URL',
  `sync_end_date` date DEFAULT NULL COMMENT '数据同步截至时间',
  `total_shares` bigint(20) DEFAULT NULL COMMENT '总股本',
  `type` char(1) NOT NULL DEFAULT 'S' COMMENT 'E:ETF;S:Stock;M:Market Data',
  `market_capital` bigint(32) unsigned DEFAULT '0' COMMENT '总市值',
  UNIQUE KEY `unique_stock` (`code`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='股票表';

DROP TABLE IF EXISTS  `stock_price_day`;
CREATE TABLE `stock_price_day` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `stock_code` varchar(255) NOT NULL COMMENT 'stock.code',
  `thedate` date NOT NULL COMMENT '交易日期',
  `pre_date` date DEFAULT NULL COMMENT '上一个交易日期',
  `next_date` date DEFAULT NULL COMMENT '下一个交易日期',
  `open` double NOT NULL DEFAULT '0' COMMENT '开盘价格',
  `close` double NOT NULL DEFAULT '0' COMMENT '收盘价格',
  `high` double DEFAULT NULL COMMENT '最高价格',
  `low` double DEFAULT NULL COMMENT '最低价格',
  `chg` double DEFAULT NULL COMMENT '昨日涨跌',
  `percent` double DEFAULT NULL COMMENT '昨日涨跌率',
  `volume` bigint(11) DEFAULT NULL COMMENT '成交量（股数）',
  `turnrate` double DEFAULT NULL COMMENT '换手率',
  `ma5` double DEFAULT NULL COMMENT '5日均线',
  `ma10` double DEFAULT NULL COMMENT '10日均线',
  `ma20` double DEFAULT NULL COMMENT '20日均线',
  `ma30` double DEFAULT NULL COMMENT '30日均线',
  `macd` double DEFAULT NULL COMMENT 'macd',
  `dif` double DEFAULT NULL COMMENT 'dif',
  `dea` double DEFAULT NULL COMMENT 'dea',
  `split` smallint(32) unsigned DEFAULT '100' COMMENT '当天是否拆股，100就是不拆,1000表示拆成1股拆10股,5表示拆成20股合成1股',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_shares_id_thedate` (`stock_code`,`thedate`)
) ENGINE=InnoDB AUTO_INCREMENT=1103005 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

DROP TABLE IF EXISTS  `stock_price_day_ex`;
CREATE TABLE `stock_price_day_ex` (
  `id` int(11) unsigned NOT NULL,
  `stock_code` varchar(32) DEFAULT NULL,
  `thedate` date DEFAULT NULL,
  `ema12` double DEFAULT NULL,
  `ema26` double DEFAULT NULL,
  `dif` double DEFAULT NULL,
  `dea` double DEFAULT NULL,
  `macd` double DEFAULT NULL,
  `macd_trend` char(1) DEFAULT NULL,
  `macd_reverse_price` double DEFAULT NULL,
  `k` double DEFAULT NULL,
  `d` double DEFAULT NULL,
  `j` double DEFAULT NULL,
  `rsi1` double DEFAULT NULL,
  `rsi2` double DEFAULT NULL,
  `rsi3` double DEFAULT NULL,
  `boll_up` double DEFAULT NULL COMMENT '布林带上轨',
  `boll_mid` double DEFAULT NULL COMMENT '布林带中轨',
  `boll_low` double DEFAULT NULL COMMENT '布林带下轨',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='股票价格信息扩展表';

DROP TABLE IF EXISTS  `stock_price_month`;
CREATE TABLE `stock_price_month` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `stock_code` varchar(255) NOT NULL COMMENT 'stock.code',
  `thedate` date NOT NULL COMMENT '起始日期',
  `pre_date` date DEFAULT NULL COMMENT '上一个交易日期',
  `next_date` date DEFAULT NULL COMMENT '下一个交易日期',
  `open` double NOT NULL DEFAULT '0' COMMENT '开盘价格',
  `close` double NOT NULL DEFAULT '0' COMMENT '收盘价格',
  `high` double DEFAULT NULL COMMENT '最高价格',
  `low` double DEFAULT NULL COMMENT '最低价格',
  `chg` double DEFAULT NULL COMMENT '昨日涨跌',
  `percent` double DEFAULT NULL COMMENT '昨日涨跌率',
  `volume` bigint(11) DEFAULT NULL COMMENT '成交量（股数）',
  `turnrate` double DEFAULT NULL COMMENT '换手率',
  `ma5` double DEFAULT NULL COMMENT '5日均线',
  `ma10` double DEFAULT NULL COMMENT '10日均线',
  `ma20` double DEFAULT NULL COMMENT '20日均线',
  `ma30` double DEFAULT NULL COMMENT '30日均线',
  `macd` double DEFAULT NULL COMMENT 'macd',
  `dif` double DEFAULT NULL COMMENT 'dif',
  `dea` double DEFAULT NULL COMMENT 'dea',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51860 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

DROP TABLE IF EXISTS  `stock_price_week`;
CREATE TABLE `stock_price_week` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `stock_code` varchar(255) NOT NULL COMMENT 'stock.code',
  `thedate` date NOT NULL COMMENT '起始日期',
  `pre_date` date DEFAULT NULL COMMENT '上一个交易日期',
  `next_date` date DEFAULT NULL COMMENT '下一个交易日期',
  `open` double NOT NULL DEFAULT '0' COMMENT '开盘价格',
  `close` double NOT NULL DEFAULT '0' COMMENT '收盘价格',
  `high` double DEFAULT NULL COMMENT '最高价格',
  `low` double DEFAULT NULL COMMENT '最低价格',
  `chg` double DEFAULT NULL COMMENT '昨日涨跌',
  `percent` double DEFAULT NULL COMMENT '昨日涨跌率',
  `volume` bigint(11) DEFAULT NULL COMMENT '成交量（股数）',
  `turnrate` double DEFAULT NULL COMMENT '换手率',
  `ma5` double DEFAULT NULL COMMENT '5日均线',
  `ma10` double DEFAULT NULL COMMENT '10日均线',
  `ma20` double DEFAULT NULL COMMENT '20日均线',
  `ma30` double DEFAULT NULL COMMENT '30日均线',
  `macd` double DEFAULT NULL COMMENT 'macd',
  `dif` double DEFAULT NULL COMMENT 'dif',
  `dea` double DEFAULT NULL COMMENT 'dea',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227221 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

SET FOREIGN_KEY_CHECKS = 1;

/* VIEWS */;
DROP VIEW IF EXISTS `v_hg`;
CREATE VIEW `v_hg` AS select `a1`.`id` AS `id`,`a1`.`stock_code` AS `stock_code`,`a1`.`thedate` AS `thedate`,`a1`.`open` AS `open`,`a1`.`split` AS `split`,`a2`.`thedate` AS `a2thedate`,`a2`.`close` AS `close`,(`a1`.`open` / `a2`.`close`) AS `a1.``open`` / a2.``close```,round((`a1`.`open` / `a2`.`close`),0) AS `r`,round((100 / round((`a1`.`open` / `a2`.`close`),0)),0) AS `s` from (`stock_price_day` `a1` left join `stock_price_day` `a2` on(((`a1`.`stock_code` = `a2`.`stock_code`) and (`a1`.`thedate` = `a2`.`next_date`)))) where (((`a1`.`open` / `a2`.`close`) > 2) and (`a1`.`open` > 1) and (`a2`.`open` > 1) and (`a1`.`split` = 100));

DROP VIEW IF EXISTS `v_hg_2`;
CREATE VIEW `v_hg_2` AS select `a1`.`id` AS `id`,`a1`.`stock_code` AS `stock_code`,`a1`.`thedate` AS `thedate`,`a1`.`open` AS `open`,`a1`.`split` AS `split`,`a2`.`thedate` AS `a2thedate`,`a2`.`close` AS `close`,(`a1`.`open` / `a2`.`close`) AS `a1.``open`` / a2.``close```,round((`a2`.`open` / `a1`.`close`),0) AS `r`,round((100 * round((`a2`.`open` / `a1`.`close`),0)),0) AS `s` from (`stock_price_day` `a1` left join `stock_price_day` `a2` on(((`a1`.`stock_code` = `a2`.`stock_code`) and (`a1`.`thedate` = `a2`.`next_date`)))) where (((`a2`.`open` / `a1`.`close`) > 2) and (`a1`.`open` > 1) and (`a2`.`open` > 1) and (`a1`.`split` = 100));